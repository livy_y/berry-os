## Berry Flip: Raspberry Pi Zero W Flipphone Project

**Overview**

This project aims to build a functional flipphone using a Raspberry Pi Zero W! It combines the classic design of a flip phone with the power and versatility of the Raspberry Pi, offering a unique mobile experience.

**Goals**

* Create a working phone with basic functionality: calling, texting and Snake ofcourse.
* Design a custom clamshell case for the Raspberry Pi and other components.
* Implement intuitive user interface and controls.

**Hardware**

- [x] Raspberry Pi Zero W
- [ ] 2.8' TFT SPI screen
- [ ] Battery
- [ ] Cellular modem (optional for cellular functionality)
- [ ] Buttons and switches for user interaction
- [ ] Speaker and microphone
- [ ] Case materials (3D printed or other)

**Software**
* **Programming language:** Rust, leveraging embedded development libraries and drivers.
* **Operating system:** Baremetal environment, booting directly into your own Rust code.
* **Communication software:** Utilize libraries like `embedded_hal` or explore direct hardware interaction for calling/texting functionalities.
* **User interface:** Text-based or minimal graphic interface optimized for a small screen and flip functionality. Rust graphics libraries like `embedded_graphics` might be considered.

**Current Status**

Planning stage: I am working out how i am going to apreach building Berry Flip.

**Contributing**

We welcome contributions from anyone interested in this project! Feel free to:

* Fork the repository and suggest improvements.
* Share ideas and suggestions for features or hardware design.
* Help write documentation or tutorials.

**License**

GPL v3
