#!/bin/bash

set -e  # Exit on any error

# Step 1: Compile the Rust code with linker script
echo -e "\e[32mCompiling Rust code...\e[0m"
cargo rustc -- -C link-arg=--script=./linker.ld

# Step 2: Convert the output to binary format
echo -e "\e[33mConverting output to binary...\e[0m"
arm-none-eabi-objcopy -O binary ./target/armv7a-none-eabi/debug/kernel ./target/kernel7.img

# Step 3: Detect and upload to FAT disk
echo -e "\e[36mDetecting FAT disk...\e[0m"

CURRENT_USER=$(whoami)

MOUNT_POINT="/run/media/$CURRENT_USER/BOOT"
if [ ! -d "$MOUNT_POINT" ]; then
  MOUNT_POINT="/media/$CURRENT_USER/BOOT"
fi

if mountpoint -q "$MOUNT_POINT"; then
  echo -e "\e[32mFAT disk found at /run/media/livy/BOOT.\e[0m"
  echo -e "\e[34mUploading kernel7.img...\e[0m"
  cp ./target/kernel7.img "$MOUNT_POINT"
  echo -e "\e[32mUpload complete.\e[0m"
else
  echo -e "\e[31mFAT disk named BOOT not found.\e[0m"
fi
