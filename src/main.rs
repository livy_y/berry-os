#![no_std]
#![no_main]

// 7E20_0000 fsel8 1<<24: set 001 turn pin 8 into an output
// 7E20_001C gpio0_set 1<<8: turn pin 8 on
// 7E20_0028 gpio0_clear 1<<8: turn pin 8 off

use core::{arch::asm, panic::PanicInfo, ptr::write_volatile};

mod boot {
    use core::arch::global_asm;

    global_asm!(".section .text._start");
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    static mut FSEL0: *mut u32 = 0x7E20_0000 as *mut u32;
    static mut GPIO0_SET: *mut u32 = 0x7E20_001C as *mut u32;
    static mut GPIO0_CLEAR: *mut u32 = 0x7E20_0028 as *mut u32;

    unsafe {
        // Turn pin2 into an output
        write_volatile(FSEL0, 1 << 24);

        loop {
            // Turn pin2 on
            write_volatile(GPIO0_SET, 1 << 8);

            for _ in 1..5000 {
                asm!("nop")
            }

            // Turn pin17 off
            write_volatile(GPIO0_CLEAR, 1 << 8);

            for _ in 1..5000 {
                asm!("nop")
            }
        }
    }
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
