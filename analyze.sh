#!/bin/bash

set -e  # Exit on any error

# Step 1: Compile the Rust code with linker script
echo -e "\e[32mCompiling Rust code...\e[0m"
cargo rustc -- -C link-arg=--script=./linker.ld

# Step 2: Display disassembly in Vim
echo -e "\e[36mDisplaying disassembly ...\e[0m"
arm-none-eabi-objdump -D ./target/armv7a-none-eabi/debug/kernel | highlight -O xterm256 --syntax arm | less -R
